import React, { Component, useEffect, useState } from 'react';
import { Text, View, TouchableOpacity, StyleSheet, ActivityIndicator, TextInput, Button, Alert, useColorScheme } from 'react-native'

const Input = (props) => {
   const [todoName, setTodoName] = useState('');
   const [addBtnTitle, setAddBtnTitle] = useState('Add');
   const isDarkMode = useColorScheme() === 'dark';

   useEffect(() => {
      // if (props.editData != {} && props.editData.value != '' && props.editData) {
      //    // Alert.alert(props.editData.value);
      //    setTodoName(props.editData.value);
      //    setAddBtnTitle('Save');
      // }
   }, [props.editData]);

   const addTodo = (e) => {
      e.preventDefault();
      if (todoName) {
         addBtnTitle === 'Add' ? props.onAdd(todoName) : props.saveIt(props.editData);
         textInput.clear();
         setAddBtnTitle('Add');
         setTodoName('');
      }
   }
   return (
      <View style={styles.footer}>
         <TextInput
            ref={input => { textInput = input }}
            placeholder='Add a todo'
            style={isDarkMode ? styles.inputContainerDark : styles.inputContainerLight}
            onChangeText={setTodoName}
            value={todoName} />
         <Button title={addBtnTitle} onPress={addTodo} style={styles.addButton} color='#8EC3B0' />
      </View>
   )
}
export default Input

const styles = StyleSheet.create({
   footer: {
      width: '100%',
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: 20,
   },
   inputContainerLight: {
      height: 50,
      paddingHorizontal: 20,
      elevation: 40,
      backgroundColor: 'white',
      flex: 1,
      marginVertical: 20,
      marginRight: 20,
      borderRadius: 30,
      borderColor: 'grey',
      borderWidth: 5,
   },
   inputContainerDark: {
      height: 50,
      paddingHorizontal: 20,
      elevation: 40,
      backgroundColor: 'black',
      flex: 1,
      marginVertical: 20,
      marginRight: 20,
      borderRadius: 30,
      borderColor: 'grey',
      borderWidth: 5,
      color: 'white',
   },
   addButton: {
      borderRadius: 20,
   }
})