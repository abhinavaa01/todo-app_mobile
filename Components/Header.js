import React, { Component, useState } from 'react';
import { Text, View, TouchableOpacity, StyleSheet, ActivityIndicator, TextInput, Button, Alert } from 'react-native'

const Header = () => {
   return (
      <View style={styles.Header}>
         <Text style={styles.headertText}>Todo App</Text>
      </View>
   )
}
export default Header

const styles = StyleSheet.create({
    Header: {
        width: '100%',
        paddingHorizontal: 20,
        marginTop: 10,
    },
    headertText: {
        fontSize: 25,
        fontWeight: 'bold',
        width: '100%',
    }
 })