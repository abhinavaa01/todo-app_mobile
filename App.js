import React, { useState } from 'react'
import { Alert, SafeAreaView, ScrollView, StyleSheet, Text, Touchable, TouchableOpacity, useColorScheme, View } from 'react-native'
import Input from './Components/Input.js'
import Header from './Components/Header.js'
import Todo from './Components/Todo.js'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack'



const App = () => {
   const [todos, setTodos] = useState([]);
   const [newEdit, setNewEdit] = useState({});
   let isDarkMode = useColorScheme() === 'dark';
   const Stack = createNativeStackNavigator();


   const addTodo = (theTodo) => {
      // e.preventDefault();
      setTodos([...todos, theTodo]);
   }


   const delTodo = (id) => {
      let finalList = todos.filter((value, index) => {
         return index !== id;
      });
      setTodos(finalList);
   }


   const editTodo = (task, id) => {
      // Alert.alert(task+' -- '+id)
      let data = {
         value: task,
         index: id
      }
      setNewEdit(data);
      // delTodo(id);
   }


   const saveTodo = (object) => {
      let newTodos = todos;
      newTodos[object.index] = object.value;
      setTodos(newTodos);
      setNewEdit({});
      // delTodo(id);
   }

   function HomeScreen() {
      return (
         <ScrollView style={styles.fullWidth}>
            {todos.map((todo, index) => {
               return <Todo data={todo} number={index} key={index} onDel={delTodo} onEdit={editTodo} />
            })}
         </ScrollView>
      );
   }

   return (
      <SafeAreaView style={isDarkMode ? styles.mainViewDark : styles.mainView}>
         <Header />
         <Input onAdd={addTodo} editData={newEdit} saveIt={saveTodo} />
         <NavigationContainer>
            <Stack.Navigator>
               <Stack.Screen name="Home" component={HomeScreen} />
            </Stack.Navigator>
         </NavigationContainer>
      </SafeAreaView>
   )
}
export default App

const styles = StyleSheet.create({
   fullWidth: {
      width: '100%'
   },
   mainView: {
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'space-between',
      backgroundColor: 'white',
      height: '100%',
   },
   mainViewDark: {
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'space-between',
      height: '100%',
   },
})