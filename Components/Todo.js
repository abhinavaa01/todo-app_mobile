import React, { Component, useState } from 'react';
import { Text, View, TouchableOpacity, StyleSheet, ActivityIndicator, TextInput, Button, Alert } from 'react-native'

const Todo = (props) => {
    let todoName = props.data;
    let id = props.number;
    return (
        <View style={styles.todoSection}>
            <View style={styles.todo}>
                <Text style={styles.todoText}>{id + 1 + '. ' + todoName}</Text>
                <View style={styles.editButton}>
                    <Button title='Edit' color='#BCEAD5' onPress={()=>props.onEdit(todoName, id)} disabled={true} />
                </View>
                <View style={styles.delButton}>
                <Button title='Delete' color='#8EC3B0' onPress={()=>props.onDel(id)} />
                </View>
            </View>
        </View>
    )
}
export default Todo

const styles = StyleSheet.create({
    todoSection: {
        // backgroundColor: 'grey',
        width: '100%',
        marginVertical: 5,
    },
    todo: {
        width: '100%',
        display: 'flex',
        alignItems: 'center',
    },
    todoText: {
        backgroundColor: '#9ED5C5',
        borderRadius: 5,
        width: '100%',
        textAlign: 'center',
        paddingVertical: 10,
        fontSize: 20,
    },
    editButton: {
        width: '100%',
        borderRadius: 20,
        marginTop:3,
    },
    delButton: {
        width: '100%',
        borderRadius: 20,
        marginVertical:3,
    }
})